# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:


git clone https://bitbucket.org/Stuntsu/stroboskop


Naloga 6.2.3:
https://bitbucket.org/Stuntsu/stroboskop/commits/2f55a139db61e54652ec0184d4883089cc68f90c

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/Stuntsu/stroboskop/commits/06d0c6c829174ca5ec697c327a42766db60824bd

Naloga 6.3.2:
https://bitbucket.org/Stuntsu/stroboskop/commits/b4d594f0289663e5b43028138cc94a5c209122a5

Naloga 6.3.3:
https://bitbucket.org/Stuntsu/stroboskop/commits/26defc84cc27015012c527674e67401ff706d689

Naloga 6.3.4:
https://bitbucket.org/Stuntsu/stroboskop/commits/1fe259b3ef054bf68c4b56f7f1ea426e0fc72ae9

Naloga 6.3.5:

git checkout master
git merge izgled
git add .
git commit -m "Merge z izgled"
git push origin master

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/Stuntsu/stroboskop/commits/eacb6b265f633310862b2355ebc10fdcaaf33c7c?at=dinamika

Naloga 6.4.2:
https://bitbucket.org/Stuntsu/stroboskop/commits/1a73bf158ce10cfa04576411926b8f166ea0de31?at=dinamika

Naloga 6.4.3:
https://bitbucket.org/Stuntsu/stroboskop/commits/0adfbb13f5ae07721dbfc9ab70d811c507e2d188?at=dinamika

Naloga 6.4.4:
https://bitbucket.org/Stuntsu/stroboskop/commits/ccd45621a23eef5b6a38854b679fb06fa1a4a49c?at=dinamika